var numSquares = 6;
var colours = [];
var pickedColour;

var h1 = document.querySelector("h1");
var squares = document.querySelectorAll(".square");
var modeButtons = document.querySelectorAll(".mode");
var messageDisplay = document.querySelector("#message");
var resetBtn = document.querySelector("#reset");
var colourDisplay = document.getElementById("colourDisplay");

init();

function init() {
    // Reset button event listeners
    resetBtn.addEventListener("click", function () {
        reset();
    });

    setupModeButtons();

    setupSquares();

    reset();
}

// Set up mode button event listeners
function setupModeButtons() {
    for (var i = 0; i < modeButtons.length; i++) {
        modeButtons[i].addEventListener("click", function () {
            modeButtons[0].classList.remove("selected");
            modeButtons[1].classList.remove("selected");
            this.classList.add("selected");
            this.textContent === "Easy" ? numSquares = 3 : numSquares = 6;
            reset();
        });
    }
}

// Set up squares event listeners
function setupSquares() {
    for (var i = 0; i < squares.length; i++) {
        squares[i].addEventListener("click", function () {
            var clickedColour = this.style.backgroundColor;
            if (clickedColour === pickedColour) {
                messageDisplay.textContent = "Correct!";
                resetBtn.textContent = "Play Again?";
                changeColours(clickedColour);
                h1.style.backgroundColor = clickedColour;
            }
            else {
                this.style.backgroundColor = "#232323";
                messageDisplay.textContent = "Try Again";
            }
        });
    }
}

function reset() {
    // generate all new colours
    colours = generateRandomColours(numSquares);
    // pick a new random colour from the array
    pickedColour = pickColour();
    // change colourDisplay to match picked colour
    colourDisplay.textContent = pickedColour;
    // set messages
    resetBtn.textContent = "New Colours";
    messageDisplay.textContent = "";
    // change colours of squares
    for (var i = 0; i < squares.length; i++) {
        if (colours[i]) {
            squares[i].style.display = "block";
            squares[i].style.backgroundColor = colours[i];
        }
        else {
            squares[i].style.display = "none";
        }
    }
    h1.style.backgroundColor = "steelblue";
}

function changeColours(colour) {
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = colour;
    }
}

function pickColour() {
    var random = Math.floor(Math.random() * colours.length);
    return colours[random];
}

function generateRandomColours(num) {
    var arr = [];
    for (var i = 0; i < num; i++) {
        arr.push(randomColour());
    }
    return arr;
}

function randomColour() {
    var r = Math.floor(Math.random() * 256);
    var g = Math.floor(Math.random() * 256);
    var b = Math.floor(Math.random() * 256);
    return "rgb(" + r + ", " + g + ", " + b + ")";
}
