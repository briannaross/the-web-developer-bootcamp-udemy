$("div").css("backgroundColor", "purple");

$("div.highlight").css("width", "200px");

$("#third").css("border", "2px solid orange");

$("div:first-of-type").css("color", "pink"); // Remember the CSS selector videos?!
$("div:first").css("color", "pink"); // jQuery method (slightly slower)
