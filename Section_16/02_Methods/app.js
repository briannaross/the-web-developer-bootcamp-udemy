$("img").css("width", "200px");

// .text() https://api.jquery.com/text/
$("h1").text("New Text!!!");
$("li").text("Rusty, Colt's dog, is adorable");

// .html() https://api.jquery.com/html/
$("ul").html("<li>I hacked your URL!</li><li>I hacked your URL!</li><li>Rusty is still adorable!</li>");
$("li").html("Google: <a href='google.com'>CLICK ME TO GO TO GOOGLE</a>");

// .attr() https://api.jquery.com/attr/
//$("img").attr("src", "http://c3.staticflickr.com/3/2418/2243463214_f32ab004af_b.jpg");
//$("input").attr("type", "checkbox");
$("img:first-of-type").attr("src", "http://c3.staticflickr.com/3/2418/2243463214_f32ab004af_b.jpg");
$("img").last().attr("src", "http://c3.staticflickr.com/3/2418/2243463214_f32ab004af_b.jpg");
$("img").attr("src", "https://upload.wikimedia.org/wikipedia/commons/8/8e/Newfoundland_Pine_Marten.jpg");

// .val() https://api.jquery.com/val/
console.log($("img:first-of-type").val("src")[0]);
console.log($("input").val()); // get value
$("input").val("Andrea"); // update value
console.log($("select").val());

// .addClass() https://api.jquery.com/addClass/
// .removeClass() https://api.jquery.com/removeClass/
// .toggleClass() https://api.jquery.com/toggleClass/
$("h1").addClass("correct");
$("h1").removeClass("correct");
$("li").addClass("wrong");
$("li").removeClass("wrong");
$("li").addClass("correct");
$("li").first().toggleClass("done");
$("li").toggleClass("done");
