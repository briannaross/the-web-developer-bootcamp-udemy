// Apply a bunch of styles in a custom object using jQuery
var styles = {
    color: "red",
    background: "pink",
    border: "2px solid purple"
}
$("h1").css(styles)

// Change all elements of a type in one go
$("li").css("color", "blue");
// ...replaces all this:
// var lis = document.querySelectorAll("li");
// for (var i = 0; i < lis.length; i++) {
//     lis[i].style.color = "blue";
// }

// Another example
$("a").css("font-size", "40px");

// Use a transient object to change css properties
$("li").css({
    fontSize: "10px",
    border: "3px dashed purple",
    backgroundColor: "rgba(89, 45, 20, 0.5)"
});
