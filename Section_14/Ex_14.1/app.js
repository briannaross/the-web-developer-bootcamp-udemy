var bgStyleToggle = false;

document.querySelector("button").addEventListener("click", function() {changeBG();});

// function changeBG() {
//     if (bgStyleToggle) {
//         document.body.classList = "defaultBG";
//     }
//     else {
//         document.body.classList = "colourBG";
//     }

//     bgStyleToggle = !bgStyleToggle;
// }

function changeBG() {
    document.body.classList.toggle("colourBG"); // Effectively does the same as above (turns colourBG class style on and off).
}
