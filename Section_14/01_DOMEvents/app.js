var buttons = document.querySelectorAll("button");
var paragraphs = document.querySelectorAll("p");

for (var i = 0; i < buttons.length; i++) {
    var p = paragraphs[i];
    console.log(p.textContent);
    buttons[i].addEventListener("click", function() {
        p.textContent = "Someone Clicked the Button!";
    });

    buttons[i].addEventListener("mouseover", function() {
        this.style.color = "darkgreen";
    });

    buttons[i].addEventListener("mouseleave", function() {
        this.style.color = "black";
    });
}
