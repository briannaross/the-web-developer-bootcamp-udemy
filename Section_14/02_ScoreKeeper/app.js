var p1Btn = document.querySelector("#p1");
var p2Btn = document.getElementById("p2"); // Alternative selector method
var resetBtn = document.getElementById("reset");
var playToSelector = document.getElementById("playToSelector");
var playTo = document.getElementById("playTo");
var p1Display = document.getElementById("p1Display");
var p2Display = document.getElementById("p2Display");
var p1Score = 0;
var p2Score = 0;
var gameOver = false;
var winningScore = 5;

p1Btn.addEventListener("click", function(){
    if (!gameOver) {
        p1Score++;
        if (p1Score === winningScore) {
            p1Display.classList.add("winner");
            gameOver = true;
        }
        p1Display.textContent = p1Score;
    }
});

p2Btn.addEventListener("click", function() {
    if (!gameOver) {
        p2Score++;
        if (p2Score === winningScore) {
            p2Display.classList.add("winner");
            gameOver = true;
        }
        p2Display.textContent = p2Score;
    }
});

resetBtn.addEventListener("click", function() {
    reset();
});

playToSelector.addEventListener("change", function() {
    playTo.textContent = playToSelector.value;
    winningScore = Number(playToSelector.value);
});

function reset() {
    p1Score = 0;
    p2Score = 0;
    p1Display.textContent = p1Score;
    p2Display.textContent = p2Score;
    p1Display.classList.remove("winner");
    p2Display.classList.remove("winner");
    gameOver = false;
};