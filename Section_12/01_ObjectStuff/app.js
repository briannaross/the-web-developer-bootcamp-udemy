var movies = [];

movies.push({ title: "The Last Jedi", rating: 4, watched: true });
movies.push({ title: "Swingin' Safari", rating: 3, watched: false });
movies.push({ title: "Red Dog", rating: 4, watched: true });
movies.push({ title: "The Dark Tower", rating: 1.5, watched: false });


movies.forEach(function(elem, i) {
    var seen = hasWatched(elem);
    var movie = getMovieTitle(elem);
    var rating = getRating(elem);
    console.log("You have " + seen + movie + " - " + rating);
});


function hasWatched(elem) {
    if (elem.watched === true) {
        return "seen ";
    }
    return "not seen ";
}

function getMovieTitle(elem) {
    return "\"" + elem.title + "\"";
}

function getRating(elem) {
    return elem.rating + " stars";
}
