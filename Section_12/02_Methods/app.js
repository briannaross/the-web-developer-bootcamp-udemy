var obj = {
    name: "Chuck",
    age: 45,
    isCool: false,
    friends: ["bob", "tina"],
    add: function(x, y) {
        return x + y;
    },
    getAge: function() {
        return this.age;
    }
}

console.log(obj.add(10, 5));
console.log(obj.getAge());

var dogSpace = {};
dogSpace.speak = function() { return "WOOF!"};
var catSpace = {};
catSpace.speak = function() { return "MIAOW!"};

