var person = {
    name: "Travis",
    age: 21,
    city: "LA"
};

var person2 = new Object;
person2.name = "Sarah";
person2.age = 18;
person2.city = "NYC";

var people = [];

people.push(person);
people.push(person2);
people.push({name: "Bob", age: 36, city: "LA"});
people.push({ stuff: "Blah" });

console.log(people[1].age);
people[1].age++;
console.log(people[1].age);
console.log(people[3].stuff);


var someObject = {
    friends: [
        { name: "Malfoy" },
        { name: "Crabbe" },
        { name: "Goyle" }
    ],
    color: "baby blue",
    isEvil: true
};

console.log(someObject.friends[0].name);
