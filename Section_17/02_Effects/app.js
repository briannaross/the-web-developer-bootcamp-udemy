$("button").on("click", function(){

    // $("div").fadeIn(1000, function () {
    //     console.log("Fade-in Completed!"); // This will execute after the fadeout finishes.
    // });

    // $("div").fadeToggle(500);

    //$("div").slideDown(500);
    //$("div").slideToggle();

    $("div").slideToggle(1000, function() {
        console.log("Slide is done!");
    });

    // $("div").fadeOut(1000);
    // console.log("Fade Completed!"); // Note that this is executed before the fadeout finishes.
   
    // $("div").fadeOut(1000, function() {
    //     console.log("Fade-out Completed!"); // This will execute after the fadeout finishes.
    //     $(this).remove(); // Removes the faded element once fade is complete, otherwise it just hangs around.
    // });

    // $(this).remove(); // The remove() placed here would remove before the fade is completed.
});
