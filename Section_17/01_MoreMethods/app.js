// // .click() - https://api.jquery.com/click/
// // Using anonymous function
// $("h1").click(function() {
//     alert("h1 clicked");
// });

// // Using standalone function
// $("button").click(changeBtnBackground);

// function changeBtnBackground() {
//     $(this).css("background-color", "pink"); // Note the jQuery version of 'this'.
//                                              // Use 'this' for the specific instance to act upon.
// }

// // .keypress() - https://api.jquery.com/keypress/
// // Passing in an event
// $("input[type='text']").keypress(function(event) {
//     if(event.which === 13) { // .which is the key code of the last typed key
//         alert("You hit ENTER");
//     }
// });

// .on() - https://api.jquery.com/on/
// .on() can be used to add listeners for all potential future elements (click() can't do this)
$("h1").on("click", function() {
    $(this).css("color", "purple");
});

$("input").on("keypress", function(){
    console.log("Key pressed!");
});

// Note: These two events can be done in CSS using the :hover psuedo-selector
$("button").on("mouseenter", function () {
    $(this).css("font-weight", "bold");
});

$("button").on("mouseleave", function () {
    $(this).css("font-weight", "normal");
});
