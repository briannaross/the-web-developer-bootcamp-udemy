// Check off specific Todos by clicking
$("ul").on("click", "li", function() { // Use .on() for dynamically created children
    $(this).toggleClass("completed");
});

// Click on X to delete Todo
$("ul").on("click", "span", function(event) {
    $(this).parent().fadeOut(500, function() {
        $(this).remove(); // Note that this 'this' is not the same as the 'this' in the calling function!
    });
    
    event.stopPropagation();
});

$("input[type='text']").keypress(function(event) {
    if (event.which === 13) { // Check for 'Enter' key
        var todoText = $(this).val(); // Get value of input field
        $(this).val(""); // Clear input field
        // Create a new li and add to ul
        $("ul").append("<li><span><i class='fa fa-trash'></i></span> " + todoText + "</li>"); // Append to all ul elements (only one in this project)
    }
});

$(".fa-plus").click(function() {
    $("input[type='text']").fadeToggle(); 
});