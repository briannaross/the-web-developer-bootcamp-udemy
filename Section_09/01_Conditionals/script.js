var age = Number(prompt("What is your age?"));

if (age < 0) {
    console.log("Error: Age cannot be below 0");
}

if (age === 21) {
    console.log("Happy 21st birthday!!");
}

if ((age % 2) === 1) {
    console.log("Your age is odd!");
}

// if ((age == 1) || (age == 4) || (age == 9) || (age == 16) || (age == 25) || (age == 36) || (age == 49) || (age == 64) || (age == 81) || (age == 100) || (age == 121))  {
if (age % Math.sqrt(age) === 0) {
    console.log("Perfect square!");
}
