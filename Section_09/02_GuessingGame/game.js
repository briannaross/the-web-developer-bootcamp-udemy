
var secretNumber = Math.round(Math.random(1000) * 1000);// 4;
var guess = -1;
var numGuesses = 0;
var maxGuesses = 7;

while (guess !== secretNumber && numGuesses < maxGuesses) {
    guess = Number(prompt("Guess a number"));

    if (guess > secretNumber) {
        alert("The secret number is lower.");
    }
    else if (guess < secretNumber) {
        alert("The secret number is higher.");
    }
    numGuesses++;
}

if (guess === secretNumber) {
    numGuesses--;
    alert("You got it right! It took you " + numGuesses + " guesses to get it right.");
}
else if (numGuesses == maxGuesses) {
    alert("Sorry, you took the maximum number of guesses. The secret number is " + secretNumber);
}

