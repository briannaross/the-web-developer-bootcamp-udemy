function isEven(num) {
    if (num % 2 === 0) {
        return true;
    }
    return false;
}

function factorial(num) {
    result = 1;
    for (i = 2; i <= num; i++) {
        result *= i;
    }
    return result;
}

function kebabToSnake(str) {
    var re = /-/g; // Regular expression - between slashes is value to be replaced, g indicates global (can also use i to ignore case)
    return String(str).replace(re, "_");
}

console.log("isEven()");
console.log(isEven(4));     // true
console.log(isEven(21));    // false
console.log(isEven(68));    // true
console.log(isEven(333));   // false

console.log("factorial()");
console.log(factorial(5));  // 120
console.log(factorial(2));  // 2
console.log(factorial(10)); // 3628800
console.log(factorial(0));  // 1

console.log("kebabToSnake()");
console.log(kebabToSnake("hello-world"));       // "hello_world"
console.log(kebabToSnake("dogs-are-awesome"));  // "dogs_are_awesome"
console.log(kebabToSnake("blah"));              // "blah"

// console.log();
