var todos = ["Item 1", "Item 2", "Item 3"];
var input = "";

while (input !== "quit") {
    input = prompt("What would you like to do?");
    if (input === "list") {
        listTodos();
    }
    else if (input === "new") {
        addTodo();
    }
    else if (input === "delete") {
        deleteTodo();
    }
}
console.log("Quitting app...");


function listTodos() {
    console.log("**********");
    todos.forEach(function (todo, i) { // Loop through an array, passing in an item and it's index to the forEach function each time.
                                       // An optional third parameter is the array itself - e.g. arr.forEach(function(elem, i, arr){...});
                                       // function() can also be a predefined function if desired, but forEach does not need that function's param list - e.g.
                                       //
                                       // function myFunc(elem, i, arr) {
                                       //     ...some code...
                                       // };
                                       //
                                       // arr.forEach(myFunc);
                                       //
        console.log(i + ": " + todo);
    });
    console.log("**********");
}

function addTodo() {
    var newTodo = prompt("Enter new Todo");
    todos.push(newTodo); // Add an item to an array.
    console.log("Added Todo");
}

function deleteTodo() {
    var i = prompt("Enter index of Todo to be deleted");
    todos.splice(i, 1); // Delete one or a range of items from an array.
    console.log("Deleted Todo");
}
