// Pass in a function
function myForEach(arr, func) {
    for (var i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
};


var colours = ["red", "orange", "yellow"];

// Pass in a predefined function
myForEach(colours, alert);

// Pass in an anonymous function
myForEach(colours, function(colour) {
    console.log(colour);
});

// Add myForEach method to the Array object
Array.prototype.myForEach = function(func) {
    for(var i = 0; i < this.length; i++) {
        func(this[i]);
    }
}

var friends = ["Charlie", "Dave", "Maddy", "Caitlin"];

// Pass in a predefined function
friends.myForEach(alert);

// Pass in an anonymous function
friends.myForEach(function(name) {
    console.log("I love " + name);
});
