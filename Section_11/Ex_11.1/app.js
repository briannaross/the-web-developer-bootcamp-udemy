function printReverse(arr) {
    for (var i = arr.length - 1; i >= 0; i--) {
        console.log(arr[i]);
    }
};

function isUniform(arr) {
    for (var i = 1; i < arr.length; i++) {
        if (arr[0] !== arr[i]) {
            return false;
        }
    }
    return true;
};

function sumArray(arr) {
    var sum = 0;
    arr.forEach(function(elem) {
        sum += elem;
    });
    return sum;
};

function max(arr) {
    var max = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
};


console.log("printReverse()");
printReverse([1, 2, 3, 4]);     // 4 3 2 1
printReverse(["a", "b", "c"]);  // c b a

console.log("isUniform()");
console.log(isUniform([1, 1, 1, 1]));        // true
console.log(isUniform([2, 1, 1, 1]));        // false
console.log(isUniform(["a", "b", "p"]));     // false
console.log(isUniform(["b", "b", "b"]));     // true

console.log("sumArray()");
console.log(sumArray([1, 2, 3]));            // 6
console.log(sumArray([10, 3, 10, 4]));       // 27
console.log(sumArray([-5, 100]));            // -95

console.log("max()");
console.log(max([1, 2, 3]));                 // 3
console.log(max([10, 3, 10, 4]));            // 10
console.log(max([-5 , 100]));                // 100
