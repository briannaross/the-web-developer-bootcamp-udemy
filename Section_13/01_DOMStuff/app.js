var h1 = document.querySelector("h1");
h1.style.color = "pink";

// var body = document.querySelector("body");
// var isBlue = false;

// setInterval(function() {
//     if (isBlue) {
//         body.style.background = "white";
//     } else {
//         body.style.background = "#3498db";
//     }
//     isBlue = !isBlue;
// }, 1000);

var tag = document.getElementsByTagName("p")[0];
var tag2 = document.getElementsByTagName("p")[1];
tag2.innerHTML = tag.innerHTML; // Retain tags inside selected tag
tag.textContent = "blah blah blah"; // Doesn't retain inner tags
//document.body.innerHTML = "<h1>Goodbye!</h1>";

var link = document.getElementsByTagName("a")[0];
link.setAttribute("href", "http://www.google.com/");
link.textContent = "Google";
link.innerHTML = "<em>Google</em>";
