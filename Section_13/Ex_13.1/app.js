document.getElementById("first");
// same as tag = document.querySelector("#first");

document.getElementsByClassName("special")[0];
// same as document.querySelector(".special");
// same as document.querySelectorAll(".special")[0];

document.getElementsByTagName("p")[0];
// same as document.querySelector("p");
// same as document.querySelectorAll("p")[0];

document.querySelector("h1 + p");
